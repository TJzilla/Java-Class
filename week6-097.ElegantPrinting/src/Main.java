
public class Main {

    public static void main(String[] args) {
        // test method here
        int[] array = {5, 1, 3, 4, 2};
        printElegantly(array);
    }

    public static void printElegantly(int[] array) {
        // Write code here
        String fancy = "";
        
        System.out.print(array[0]);
        for (int i = 1 ; i < array.length ; i++) {
            if (array.length == i) {
                fancy += array[i];
            } else {
                fancy += ", " + array[i];
            }
        }
        System.out.println(fancy);
    }
}
