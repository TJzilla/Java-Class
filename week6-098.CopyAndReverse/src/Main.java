
import java.util.Arrays;


public class Main {

    public static void main(String[] args) {

    }

    public static int[] copy(int[] array) {
        int[] copy = new int[array.length];

        for (int num = 0  ; num < array.length ; num++) {
            copy[num] = array[num];
        }
        return copy;
    }
    
    public static int[] reverseCopy(int[] array) {
        int[] copyReversed = new int[array.length];
        int counter = 0;
        
        for (int num = array.length - 1  ; num >= 0 ; num--) {
            copyReversed[counter] = array[num];
            counter++;
        }
        return copyReversed;
    }

}
