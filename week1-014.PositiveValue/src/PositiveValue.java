
import java.util.Scanner;

public class PositiveValue {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        
       System.out.println("Type a number: "); 
       int n = Integer.parseInt(reader.nextLine());
       
        if (n > 0) {
            System.out.println("This number is positive.");
        } else {
            System.out.println("This number is not positive.");
        }
    }
}
