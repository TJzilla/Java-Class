
import java.util.Scanner;

public class SumOfThreeNumbers {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        int sum = 0;
        int read; // store numbers read form user in this variable


        // Write your program here
        // Use only variables sum and read
        System.out.println("Number one: ");
        read = Integer.parseInt(reader.nextLine());
        sum = read;
        System.out.println("");
        
        System.out.println("Number two: ");
        read = Integer.parseInt(reader.nextLine());
        sum = sum + read;
        
        System.out.println("Number three: ");
        read = Integer.parseInt(reader.nextLine());
        sum = sum + read;

        System.out.println("Sum: " + sum);
    }
}
