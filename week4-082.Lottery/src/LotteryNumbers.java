import java.util.ArrayList;
import java.util.Random;

public class LotteryNumbers {
    private ArrayList<Integer> numbers;
    private Random rand;
    
    public LotteryNumbers() {
        this.rand = new Random();
        this.drawNumbers();
    }

    public ArrayList<Integer> numbers() {
        return this.numbers;
    }

    public void drawNumbers() {
        // We'll format a list for the number
        this.numbers = new ArrayList<Integer>();
        
        int i = 0;
        while (i < 7) {
            int number = rand.nextInt(39) + 1;
            if (containsNumber(number) == false) {
                numbers.add(number);
                i++;
            }
        }
        
    }

    public boolean containsNumber(int number) {
        // Test here if the number is already in the drawn numbers
        if (this.numbers.contains(number)) {
            return true;
        }
        return false;
    }
}
