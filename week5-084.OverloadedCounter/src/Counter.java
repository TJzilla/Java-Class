
public class Counter {
    
    private int number;
    private boolean check;
    
    public Counter(int startingValue, boolean check) {
        this.check = check;
        this.number = startingValue;
    }
    
    public Counter(int startingValue) {
        this(startingValue, false);
    }
    
    public Counter(boolean check) {
        this(0 , check);
    }
    
    public Counter() {
        this(0);
    }
    
    public int value () {
        return this.number;
    }
    
    public void increase() {
        this.increase(1);
    }
    
    public void increase(int amount) {
        if (amount >= 0) {
            this.number += amount;
        }        
    }
    
    public void decrease() {
        this.decrease(1);
    }
    
    public void decrease(int amount) {
        if (amount < 0) {
            return;
        }
        if (this.check == true && (this.number - amount) <= 0) {
            this.number = 0;
        } else {
            this.number -= amount;
        }
    }    

}
