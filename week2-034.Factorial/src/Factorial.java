import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        
        int answer = 1;
        int counter = 1;
        int numOfTimes = Integer.parseInt(reader.nextLine());
        
        while (counter <= numOfTimes) {
            System.out.println(answer);
            answer *= counter;
            counter++;
        }
        System.out.println(answer);
    }
}
