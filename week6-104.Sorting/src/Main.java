
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
    // implement your program here
        // do not put all to one method/class but rather design a proper structure to your program

        // Your program should use only one Scanner object, i.e., it is allowed to call 
        // new Scanner only once. If you need scanner in multiple places, you can pass it as parameter
        int[] values = {8, 3, 7, 9, 1, 2, 4};
        sort(values);

    }

    public static int smallest(int[] array) {
        return smallest(array, 0);
    }

    public static int smallest(int[] array, int index) {

        int smallest = array[index];
        while (index <= array.length - 1) {
            if (array[index] < smallest) {
                smallest = array[index];
            }
            index++;
        }
        return smallest;
    }

    public static int indexOfTheSmallest(int[] array) {
        return indexOfTheSmallestStartingFrom(array, 0);
    }

    public static int indexOfTheSmallestStartingFrom(int[] array, int index) {
        int smallest = smallest(array, index);
        while (index <= array.length - 1) {
            if (array[index] == smallest) {
                return index;
            }
            index++;
        }
        return index;
    }

    public static void swap(int[] array, int index1, int index2) {
        int index = array[index1];
        int indexe = array[index2];
        array[index1] = indexe;
        array[index2] = index;
    }

    public static void sort(int[] array) {
        for (int i = 0; i <= array.length - 1; i++) {
            swap(array, indexOfTheSmallestStartingFrom(array, i), i);
            System.out.println(Arrays.toString(array));
        }
    }
}
