
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner reader = new Scanner(System.in);
        BirdDatabase birds = new BirdDatabase();

        while (true) {
            System.out.print("? ");

            String choice = reader.nextLine();

            if (choice.equals("Add")) {
                System.out.print("Name: ");
                String choice1 = reader.nextLine();
                System.out.print("Latin Name: ");
                String choice2 = reader.nextLine();
                birds.addBird(choice1, choice2);
            } else if (choice.equals("Observation")) {
                System.out.print("What is observed:? ");
                String name = reader.nextLine();
                birds.observeBird(name);
            } else if (choice.equals("Statistics")) {
                birds.statistics();
            } else if (choice.equals("Show")) {
                System.out.print("What? ");
                String name = reader.nextLine();
                birds.show(name);
            } else if (choice.equals("Quit")) {
                break;
            }
        }
    }

}
