
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author TJ
 */
public class BirdDatabase {
    
    private ArrayList<Bird> birds;
    
    
    public BirdDatabase () {
        this.birds = new ArrayList<Bird>();
    }
    
    public void addBird(String name, String latinName) {
        birds.add(new Bird(name, latinName));
    }
    
    public void observeBird(String name) {
        for (Bird b : birds) {
            if(name.equals(b.getName())) {
               b.observe();
               return;
            }
        }
        System.out.println("Is not a bird!");
    }
    
    public void statistics() {
        for (Bird b : birds) {
            System.out.println(b);
        }
    } 
    
    public void show (String bird) {
        for (Bird b : birds) {
            if(b.getName().equals(bird)) {
                System.out.println(b);
                break;
            }
        }
    }
    
}
