import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        Grade grade = new Grade();
        // implement your program here
        // do not put all to one method/class but rather design a proper structure to your program
        
        // Your program should use only one Scanner object, i.e., it is allowed to call 
        // new Scanner only once. If you need scanner in multiple places, you can pass it as parameter
        
        System.out.println("Grade Distribution:");
        while(true) {
            int test = Integer.parseInt(reader.nextLine());
            if (test != -1) {
                grade.addGrade(test);
            } else {
                break;
            }
        }
        
        grade.printGrades();
        grade.gradeDist();
    }
}
