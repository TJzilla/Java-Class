
import java.util.ArrayList;
import java.util.Iterator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author TJ
 */
public class Grade {

    private ArrayList<Integer> grades;

    public Grade() {
        this.grades = new ArrayList<Integer>();
    }

    public void addGrade(int grade) {
        this.grades.add(grade);
    }

    public int giveGrade(int grade) {
        if (grade < 0 || grade > 60) {
            return 6;
        } else if (grade < 30) {
            return 0;
        } else if (grade < 35) {
            return 1;
        } else if (grade < 40) {
            return 2;
        } else if (grade < 45) {
            return 3;
        } else if (grade < 50) {
            return 4;
        } else {
            return 5;
        }
        
    }

    public void gradeDist() {
        double test = 0;
        int zeroTo5 = 0;
        
        
        for (int grade : this.grades) {
            
            if(giveGrade(grade) != 6) {
                zeroTo5++;
            }
            
            if(giveGrade(grade) > 0 && giveGrade(grade) < 6) {
                test++;
            }           
        }
        System.out.println("Acceptance percentage: " + (test / zeroTo5) * 100);
    }
    
    public void printGrades() {
        int i = 5;
        System.out.println("Grade Distrabution:");
        while (i >= 0) {
            System.out.print(i +": ");
            for(int list : this.grades) {
                if(giveGrade(list) == i) {
                    System.out.print("*");
                }
            }
            i--;
            System.out.println("");
        }
    }
}
