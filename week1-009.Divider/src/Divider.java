
import java.util.Scanner;

public class Divider {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        // Implement your program here. Remember to ask the input from user.
        System.out.println("Type a number:");
        int num1 = Integer.parseInt(reader.nextLine());
        System.out.println("Type another number:");
        int num2 = Integer.parseInt(reader.nextLine());
        
        Double divide = (double)num1 / (double)num2;
        String toPrint = "Division: " + num1 + " / " + num2 + " = " + divide;
        System.out.println("");
        System.out.println(toPrint);
    }
}
