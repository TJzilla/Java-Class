
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author TJ
 */
public class Team {

    private String name;
    private ArrayList<Player> players;
    private int maxSize;

    public Team(String name) {
        this.name = name;
        this.players = new ArrayList<Player>();
        this.maxSize = 16;
    }

    public String getName() {
        return this.name;
    }

    public void setMaxSize(int size) {
        this.maxSize = size;
    }
    
    public int size() {
        return this.players.size();
    }

    public void addPlayer(Player player) {

        if (this.players.size() < this.maxSize) {
            this.players.add(player);
        }
    }

    public void printPlayers() {
        for (Player list : this.players) {
            System.out.println(list);
        }
    }
    
    public int goals () {
        int goals = 0;
        for (Player list : this.players) {
            goals += list.goals();
        }
        return goals;
    }
    
}
