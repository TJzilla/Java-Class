
public class Apartment {

    private int rooms;
    private int squareMeters;
    private int pricePerSquareMeter;

    public Apartment(int rooms, int squareMeters, int pricePerSquareMeter) {
        this.rooms = rooms;
        this.squareMeters = squareMeters;
        this.pricePerSquareMeter = pricePerSquareMeter;
    }
    
    public boolean larger(Apartment otherApartment) {
        return this.squareMeters > otherApartment.squareMeters;
    }
    
    public int priceDifference(Apartment otherApartment) {
        int total1 = (this.pricePerSquareMeter * this.squareMeters);
        int total2 = (otherApartment.pricePerSquareMeter * otherApartment.squareMeters);
        
        if (total1 < total2) {
            return total2 - total1;
        }
        return total1 - total2;
    }
    
    public boolean moreExpensiveThan(Apartment otherApartment) {
        if (this.pricePerSquareMeter * this.squareMeters > otherApartment.pricePerSquareMeter * otherApartment.squareMeters) {
            return true;
        }
        return false;
    }
    
}
