
import java.util.Scanner;

public class SumOfThePowers {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        
        int power = 0;
        int sumOfPowers = 0;
        
        System.out.println("Type a number: ");
        int maxPower = Integer.parseInt(reader.nextLine());
        while (power <= maxPower) {
            sumOfPowers +=(int)Math.pow(2, power);
            power++;
        }
        System.out.println("");
        System.out.println("The result is " + sumOfPowers);
    }
}
