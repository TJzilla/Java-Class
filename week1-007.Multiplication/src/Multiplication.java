public class Multiplication {

    public static void main(String[] args) {

        int a = 277;
        int b = 111;
        int equals = a * b;

        // Program your solution here. Remember to use variables a and b!
        System.out.println(a + " * " + b + " = " + equals);
    }

}
