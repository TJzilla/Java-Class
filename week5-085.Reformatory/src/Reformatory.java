public class Reformatory {

    public Reformatory() {
        this.timesWeighed = 0;
    }

    private int timesWeighed;
    
    public int weight(Person person) {
        // return the weight of the person
        this.timesWeighed++;         
        return person.getWeight();
    }

    public void feed(Person person) {
        person.setWeight(person.getWeight() + 1);

    }

    public int totalWeightsMeasured() {
        return this.timesWeighed;
    }

}
