
public class MyDate {

    private int day;
    private int month;
    private int year;

    public MyDate(int day, int montd, int year) {
        this.day = day;
        this.month = montd;
        this.year = year;
    }

    public String toString() {
        return this.day + "." + this.month + "." + this.year;
    }

    public boolean earlier(MyDate compared) {
        if (this.year < compared.year) {
            return true;
        }

        if (this.year == compared.year && this.month < compared.month) {
            return true;
        }

        if (this.year == compared.year && this.month == compared.month
                && this.day < compared.day) {
            return true;
        }

        return false;
    }

    public int differenceInYears(MyDate comparedDate) {
        int yearDiff = 0;
        
        if (comparedDate.earlier(this)) {
            yearDiff = this.year - comparedDate.year;
            
            if (comparedDate.month > this.month && yearDiff != 0) {
                yearDiff--;
            } else if (comparedDate.month == this.month && comparedDate.day > this.day && yearDiff != 0) {
                yearDiff--;
            }
            return yearDiff;
        } else {
            yearDiff = comparedDate.year - this.year;
            
            if (this.month > comparedDate.month && yearDiff != 0) {
                yearDiff--;
            } else if (this.month == comparedDate.month && this.day > comparedDate.day && yearDiff != 0) {
                yearDiff--;
            }
            return yearDiff;
        }
    }
}
