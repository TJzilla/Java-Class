
import java.util.ArrayList;

public class Library {

    private ArrayList<Book> books;

    public Library() {
        this.books = new ArrayList<Book>();
    }

    public void addBook(Book newBook) {
        this.books.add(newBook);
    }

    public void printBooks() {
        for (Book list : this.books) {
            System.out.println(list);
        }
    }

    public ArrayList<Book> searchByTitle(String title) {
        ArrayList<Book> found = new ArrayList<Book>();

        for (Book listing : this.books) {
            if (StringUtils.included(listing.title(), title)) {
                found.add(listing);
            }
        }
        return found;
    }

    public ArrayList<Book> searchByPublisher(String publisher) {
        ArrayList<Book> found = new ArrayList<Book>();

        for (Book listing : this.books) {
            if (StringUtils.included(listing.publisher(), publisher)) {
                found.add(listing);
            }
        }
        return found;
    }

    public ArrayList<Book> searchByYear(int year) {
        ArrayList<Book> found = new ArrayList<Book>();

        for (Book listing : this.books) {
            if (listing.year() == (year)) {
                found.add(listing);
            }
        }
        return found;
    }

}
