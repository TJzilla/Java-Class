import java.util.Scanner;

public class GreaterNumber {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        
        System.out.println("Type the first number: ");
        int i = Integer.parseInt(reader.nextLine());
        System.out.println("Type second number: ");
        int j = Integer.parseInt(reader.nextLine());
        
        if (i > j) {
            System.out.println("Greater number: " + i);
        } else if (j > i) {
            System.out.println("Greater number: " + j);
        } else {
            System.out.println("The numbers are equal!");
        }
    
    }
}
