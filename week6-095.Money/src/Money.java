
public class Money {

    private final int euros;
    private final int cents;

    public Money(int euros, int cents) {

        if (cents > 99) {
            euros += cents / 100;
            cents %= 100;
        }

        this.euros = euros;
        this.cents = cents;
    }

    public int euros() {
        return euros;
    }

    public int cents() {
        return cents;
    }

    public Money plus(Money added) {
        int euroSum = this.euros + added.euros;
        int centSum = this.cents + added.cents;

        if (centSum > 99) {

            euroSum += centSum / 100;
            centSum %= 100;

        }
        return new Money(euroSum, centSum);
    }
    
    public boolean less(Money compared) {
        if (this.euros < compared.euros) {
            return true;
        }
        
        if (this.euros == compared.euros && this.cents < compared.cents) {
            return true;
        }
        return false;
    }

    public Money minus(Money compared) {
        int euroSum = this.euros - compared.euros;
        int centSum = this.cents - compared.cents;

        if (centSum < 0) {
            euroSum--;
            centSum += 100;
        }
        if (euroSum < 0) {
            return new Money(0, 0);
        }
        return new Money(euroSum, centSum);
    }

    @Override
    public String toString() {
        String zero = "";
        if (cents < 10) {
            zero = "0";
        }

        return euros + "." + zero + cents + "e";
    }

}
