import java.util.Random;

public class PasswordRandomizer {
    // Define the variables
    private Random rand;
    private int length;

    public PasswordRandomizer(int length) {
        // Initialize the variable
        rand = new Random();
        this.length = length;
    }

    public String createPassword() {
        
        String password = "";
        int i = this.length;
        while (i > 0) {           
            char symbol = "abcdefghijklmnopqrstuvwxyz".charAt(this.rand.nextInt(26));
            password += symbol;
            i--;
        }
        
        return password;
    }
}
