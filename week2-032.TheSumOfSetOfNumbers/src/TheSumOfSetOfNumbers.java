
import java.util.Scanner;

public class TheSumOfSetOfNumbers {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        
        System.out.println("Until what?");
        int i = Integer.parseInt(reader.nextLine());
        int result = 0;
        int j = 0;
        
        while (j <= i) {
            result += j;
            System.out.println(result);
            j++;
        }
        System.out.println("Sum is " + result);
    }
}
