
import java.util.Scanner;


public class UpToCertainNumber {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        
        // Write your code here
        int counter = 1;
        
        System.out.println("Up to what number?");
        int max = Integer.parseInt(reader.nextLine());
        
        while (counter <= max) {
            System.out.println(counter);
            counter++;
        }
    }
}
