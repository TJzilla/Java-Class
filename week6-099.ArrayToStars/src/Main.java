
public class Main {

    public static void main(String[] args) {
        // test the method here
        int[] array = {5, 1, 3, 4, 2};
        printArrayAsStars(array);
    }

    public static void printArrayAsStars(int[] array) {
        // write code here
        for (int i = 0 ; array.length > i ; i++) {
            
            for (int j = 0 ; array[i] > j ; j++ ) {
                System.out.print("*");
            }
            System.out.println();           
        }
    }
}
