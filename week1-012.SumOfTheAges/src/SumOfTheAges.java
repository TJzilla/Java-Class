
import java.util.Scanner;

public class SumOfTheAges {

    static String name = "";
    static int age = 0;
    
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        // Implement your program here
        System.out.println("Type your name: ");
        name = reader.nextLine();
        System.out.println("Type your age: ");
        age = Integer.parseInt(reader.nextLine());
        System.out.println("");
        
        System.out.println("Type your name: ");
        name = name + " and " + reader.nextLine();
        System.out.println("Type your age: ");
        age = age + Integer.parseInt(reader.nextLine());
        System.out.println("");
        
        System.out.println(name + " are " + age + " years old in total.");
    }
}
